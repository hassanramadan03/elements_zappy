process.env.NODE_ENV = 'test'
const chai = require('chai')
const should = chai.should()
const request = require('supertest')
const app = require('../app').app
const asserts = require('chai').assert;
const {
    savePostsInDB,
    handleFBPosts
}=require('./../buisness_modules/fb/service');

describe('__User API Integration Tests__', () => {
    describe('- GET /fb/allPosts', () => {
        it('should return all posts stored in the DB', done => {
            request(app)
                .get('/fb/allPosts')
                .expect('Content-Type', /json/)
                .expect(200)
                .end((err, res) => {
                    should.not.exist(err)
                    res.body.should.include.keys('allPosts', 'ok')
                    done()
                })
        })
    })

})

describe('save posts in DB',()=>{
    it('should store posts in DB that are not stored before and return true',async()=>{
        const post={id:'123','created_time':new Date(),message:'example text for test'};
        const posts=[post]
        let result =await savePostsInDB(posts)
        asserts.equal(result,true);
    })
})
 
describe('handleFBPosts ',async()=>{
    it('should get all posts from my wall and store them in DB and Return true ',async()=>{
        let result = await handleFBPosts()
        
        asserts.equal(result,true);
    })
})
