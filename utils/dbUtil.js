const Twitter = require('../models/Twitter')
const { mongoose } = require('mongoose')

const mockTweet = {
    id: '12345',
    text: 'this is the text of tweet' ,
    create_at:new Date()
}
const cleanDB = () => {
    logger.log('... cleaning the DB')
    const cleanPromises = [Twitter].map(model => {
        return model.remove().exec()
    })
    return Promise.all(cleanPromises)
}

const seedTweet = async () => {
    const {id,ceated_at,text}=mockTweet;
    const _tweet = new Twitter({id,ceated_at,text})
    return await _tweet.save()
}

const getAllTweets = async () => {
    const allTweets = await Twitter.find({})
     return allTweets;
}
 
const closeConnection = () => mongoose.connection.close()

module.exports = {
    cleanDB,
    seedTweet,
    getAllTweets,
    closeConnection
}
