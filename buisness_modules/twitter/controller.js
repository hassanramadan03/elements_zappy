require('dotenv').config()
const TwiterService=require('./service')

const handleTWPosts=async()=>{
   try {
        TwiterService.handleTWPosts();
   } catch (error) {
       
   }
}
const getAllTwts=async(req,res)=>{
    try {
        const allTwts=await TwiterService.getAllTwts();
        res.status(200).send(allTwts);
    } catch (error) {
        res.send(error)
        
    }
}
const addTweet=async(req,res)=>{
    try {
        const _tweet=req.body.tweet;
        const tweet=await TwiterService.addTweet(_tweet);
        res.status(200).json(tweet);
    } catch (error) {
        res.status(200).send(error)
    }
}

module.exports = {
    handleTWPosts,
    getAllTwts,
    addTweet
}