const Twiter=require('../../models/Twiter')
 const {twitter_config}=require('../../config/config')

const getAllTwts=()=>{
   return new Promise(async(resolve,reject)=>{
       try {
            const allTwts=await Twiter.find({})
            resolve({ok:true,allTwts})
       } catch (error) {
            reject({ok:false,message:error})
       }
   })
}
const saveTWsInDB=(TWS)=>{
  return new Promise(async(resolve,reject)=>{
    try {
        const savedTwts= TWS.map(async(twt)=>{
             const {id,text,created_at}=twt;
             const isExisted=await Twiter.findOne({id});
             if(!isExisted){
                  const _twt=new Twiter({id,text,created_at})
                  const saved= await _twt.save();
             } 
          })
         if(savedTwts) resolve(true)
     } catch (error) {
        resolve(false);
     }
  })
}
const addTweet=(status)=>{
    return new Promise(async(resolve,reject)=>{
        try {
              const T = twitter_config;
              let tweeted=await T.post('statuses/update', { status  });
              if(tweeted)resolve({ok:true,message:'your tweet was added'});
            
        } catch ({message}) {
            reject({ok:false,message})
        }
    })
 }
 const handleTWPosts=async()=>{
    try {
     var T = twitter_config;
     const {data}=await T.get('statuses/user_timeline', { count: 200 });
     const saved= await saveTWsInDB(data);
     return saved;
    } catch (error) {
        return false;
    }
 }

module.exports={
    saveTWsInDB ,
    getAllTwts,
    addTweet,
    handleTWPosts
}
 
