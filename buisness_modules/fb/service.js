const fb = require('../../models/FB')
const FB=require('fb')
const access_token = process.env.FB_ACCESS_TOKEN
FB.setAccessToken(access_token);


const handleFBPosts = async () => {
    try {
        let {  data } = await FB.api('me/feed', 'get')
        const _savePostsInDB = await savePostsInDB(data)
        return _savePostsInDB;
    } catch (error) {
        console.log('Error validating access token');
        return false;
    }
}
const getAllPosts = () => {
    return new Promise(async (resolve, reject) => {
        try {
            const allPosts = await fb.find({})
            resolve({  ok: true,  allPosts  })
        } catch (error) {
            reject({ ok: false,  message: error })
        }
    })
}
const savePostsInDB = async (Posts) => {
    try {
        let saved = Posts.map(async (Post) => {
            const {  id,  message,  created_time } = Post;
            const isExisted = await fb.findOne({  id });
            if (!isExisted&& message && created_time) {
                const _Post = new fb({ id, text:message, created_at:created_time })
                const saved = await _Post.save();
                console.log(saved);
    
            }
        })
        
        if (saved) return true;
    } catch (error) {
        return false;
    }
}
 

module.exports = {
    handleFBPosts,
    savePostsInDB,
    getAllPosts,
}