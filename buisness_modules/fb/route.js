const express = require('express');
const router = express.Router();
const {getAllPosts} = require('./controller');

router.get('/allPosts', getAllPosts );

module.exports = router;
