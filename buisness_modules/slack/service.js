require('dotenv').config()
const request = require('request-promise');
const {slackChannelInfo}=require('../../config/config')

const isMarketingChannel = async (id) => {
   return new Promise(async(resolve,reject)=>{
    try {
        const token = process.env.SLACK_ACCESS_TOKEN;
        let uri = slackChannelInfo(token,id)
        var options = {
            method: 'get',
            uri: uri,
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            json: true // Automatically stringifies the body to JSON
        };
        const channelInfo = await request(options);
        if(channelInfo.ok){
            const {purpose}=channelInfo.channel;
            (purpose && purpose.value.toLowerCase().includes('market'))?resolve(true):resolve(false)
        }else resolve(false);
    } catch (error) {
        console.log(error);
        resolve(false)
    }
   })
}


module.exports = {
    isMarketingChannel
}
