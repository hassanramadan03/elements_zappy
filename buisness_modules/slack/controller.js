const {    handleTWPosts } = require('../twitter/service');
const {    handleFBPosts } = require('../fb/service');
const { isMarketingChannel } = require('./service')
var io=require('../../app').io;
io.on('connect',socket=>{
    console.log('=========================');
    console.log(`==== ${socket.id} connected`);
    console.log('=========================');
})
const handeSlackEvents = async(event) => {
    try {
        const IsMarketing =await isMarketingChannel(event.channel)
        console.log({IsMarketing});
        if (IsMarketing ) {
            let message = event.text.toLowerCase();
            if (/\sgo\s/i.test(` ${message} `)) {
                console.log({  message });
                io.emit('goHitted',{go:'go word was hitted by some one on marketing Channel !'})
                handleTWPosts();
                handleFBPosts()
                
            }
        }
    } catch (error) {
        console.log(error);

    }
};


module.exports = {
    handeSlackEvents
};