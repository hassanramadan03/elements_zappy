import { FeedService } from './../_services/feed.service';
import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { FBService } from '../_services/fb.service';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import * as moment from 'moment'
export interface DriverData {
  id: string;
  created_at: string;
  text: string;
}

@Component({
  selector: 'app-facebook',
  templateUrl: './facebook.component.html',
  styleUrls: ['./facebook.component.css']
})

export class FaceBookComponent {
  displayedColumns: string[] = [ 'text', 'created_at' ];
  spinner: boolean = true;
  dataSource: MatTableDataSource<DriverData>;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  id: string;
  created_at: string;
  text: string;
  errorMessage: string = '';
  isError: Boolean = false;
  constructor(  private FBService: FBService, public dialog: MatDialog,private FeedService:FeedService) {
    this.getPosts()
    this.FeedService.getMsgs().subscribe(goHitter=>{
      console.log(goHitter);
      this.getPosts()
    })
    // Assign the data to the data source for the table to render

  }
   
  getPosts() {
    this.FBService.getAllPosts().subscribe((drivers: any) => {
      this.dataSource = new MatTableDataSource(drivers)
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    }
    );
  }
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
   
  formatTime(date){
      var unix = Math.round(+new Date(date)/1000);
      return moment.unix(unix ).startOf('hour').fromNow();
     
   }

}

 