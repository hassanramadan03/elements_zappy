import { FeedService } from './../_services/feed.service';
import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { TwitterService } from '../_services/twitter.service';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

import * as moment from 'moment'
export interface DriverData {
  id: string;
  created_at: string;
  text: string;
}

@Component({
  selector: 'app-twitter',
  templateUrl: './twitter.component.html',
  styleUrls: ['./twitter.component.css']
})

export class TwitterComponent implements OnInit  {
  displayedColumns: string[] = [ 'text', 'created_at' ];
  spinner: boolean = true;
  dataSource: MatTableDataSource<DriverData>;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  id: string;
  created_at: string;
  text: string;
  phone: string;
  errorMessage: string = '';
  isError: Boolean = false;
  constructor ( private TwitterService: TwitterService,private FeedService:FeedService, public dialog: MatDialog) {
    this.getTweets();
    
  }
  ngOnInit(){
    this.FeedService.getMsgs().subscribe(goHitter=>{
      console.log({goHitter});
      this.getTweets()
    })
  }
  getTweets(){
    this.TwitterService.getAllTwts().subscribe((tweets: any) => {
      this.dataSource = new MatTableDataSource(tweets)
      this.dataSource =this.dataSource ;
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    }
    );
  }
  openDialog(): void {
    const dialogRef = this.dialog.open(DialogOverviewExampleDialog, {
      width: '600px',
      data: { id: this.id, created_at: this.created_at  }
    });

    dialogRef.afterClosed().subscribe(async (result) => {
      if (!result.text) return alert(`you didn't type any thing ..!`)
      else {
        try {
          console.log(result.text + ': is created :) ')
           this.TwitterService.addText(result.text)
        } catch ({ message }) {
          console.log(message);
          this.errorMessage = message;
          this.isError = true;
        }
      }
    });
  }
   
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
  formatTime(date){
      var unix = Math.round(+new Date(date)/1000);
      return moment.unix(unix ).startOf('day').fromNow();
   }

}

@Component({
  selector: 'dialog-overview-example-dialog',
  templateUrl: 'dialog.html',
  styleUrls: ['./twitter.component.css']
})
export class DialogOverviewExampleDialog {

  constructor(
    public dialogRef: MatDialogRef<DialogOverviewExampleDialog>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) { }

  onNoClick(): void {
    this.dialogRef.close();
  }
  onSubmit(data){
    console.log(data);
  }

}
export interface DialogData {
  text: string;
  created_at: string;
  id: string;
}