import { ApiUrlService } from "./api_url.service";
import 'rxjs/Rx';
import 'rxjs/add/operator/map'
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { SharedService } from "./shared.service";

@Injectable()
export class TwitterService {
    constructor(private SharedService:SharedService,private http: HttpClient, private apiUrlService: ApiUrlService) {
    }
    getAllTwts() {

        let header:any=this.apiUrlService.getHeaders();
        return this.http.get(this.apiUrlService.getApiUrl().getAllTwts,header ).map((res:any)=>res.allTwts)
    }
    addText(tweet){
        return fetch(this.apiUrlService.getApiUrl().addTweet, {
            method: "POST",  
            headers: {  "Content-Type": "application/json"  },
            body: JSON.stringify({tweet })  
        }).then((res:any)=>{
            if(res.ok){
                this.SharedService.openSnackBar('tweet is already created','See Twitter account !')
            }
        })
        
    }
}
