import { Injectable } from "@angular/core";
import "rxjs/add/operator/map";
import { WebSocketService } from '../_services/webSocket.service'
import { Observable, Subject } from 'rxjs/Rx'
@Injectable()
export class FeedService {
    httpOptions: {};
    goHitter: Subject<any>;
    constructor( private webService: WebSocketService) {
        // this.httpOptions = apiUrlService.getApiUrl().genericMethod;
        this.goHitter = <Subject<any>>this.webService
            .connectGoHitted()
            .map((response: any): any => {
                return response
            })
    }
    getMsgs() {
        return this.goHitter;
    }

}
